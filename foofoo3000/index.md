---
title: 'FooFoo3000, a Discord bot'
slug: 'foofoo3000'
short: "Simple Discord bot that manages channels for the server and roles for students in the Future Factory 2024 course's Discord Server"
tech: ['TypeScript', 'Bun', 'Discord']
repo: ['https://gitlab.labranet.jamk.fi/AB6225/foofoo-3000']
---

# FooFoo3000 Discord bot

![FooFoo3000](foofoo3000/assets/thumb.png)

I offered to help out as a peer-coach in the Spring 2024 implementation of Future Factory. We made a Discord for the student teams to communicate
with each other and external stakeholders. Since it would have been a massive undertaking create teams, roles and channels for ~200 students, let alone
putting the students in those teams, a Discord bot was made!

It comes with two very simple commands, a /create &lt;TEAM&gt; commands, that allows administrators to create the channels and role required for the team easily.
And /join &lt;TEAM&gt;, which allows anyone to join an existing team.

Naturally, simple safety checks were made to prevent students joining teams not meant for them, 
and that students don't have rights to use administrator commands.

Overall I'm proud of even this simple program, due to the turnover rate.
The bot took maybe 12 hours from idea to deployment, even with Discord API and libraries being completely new territory for me.
