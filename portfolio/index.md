---
title: 'Portfolio'
slug: 'portfolio'
short: "The website you're on!"
tech: ['Svelte', 'TypeScript', 'Tailwind CSS', 'Simple Icons']
repo: ['https://gitlab.com/Iusbus/portfolio']
---

<!-- Example file -->

# Svelte based portfolio

This site uses Svelte, TypeScript and Tailwind CSS to deliver a simple but nice to look at web based portfolio.

The purpose of this site is to serve as a platform to get to know about me, what I've done, and how to get in contact.

The site has some neat features, such as:
 * Generating cards and project pages from Markdown files 
 * Listing technologies in projects utilizing shield.io badges
 * Serving my contact information from a serverless API to mitigate spam
 * Lazy loading images with an image <1kb in size as placeholder
